<?php
ini_set('max_execution_time', 0);
set_time_limit(1800);
ini_set('memory_limit', '-1');

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "thesis";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$sql = "SELECT * FROM `tables`";
$result = $conn->query($sql);


$data = [];

if ($result->num_rows > 0) {
    while($rows = $result->fetch_assoc()){
        if(file_exists("img/".$rows['name'].".png")){
			copy("img/".$rows['name'].".png", "pro/".$rows['name'].".png");
		} else{
			echo "File doesn't exist!";
		}
    }

} else {
    echo json_encode([
        'success'=>false,
        'msg'=> 'You have done all of the links'
    ]);exit;
}
$conn->close();

$json = json_encode([
    'success'=>true,
    'data'=>$data
]);
echo $json;
?>