$('a.next').on('click', function(){
    let result = null;
    let item = $('.carousel-item.active');
    let name = item.attr('data-name');
    let table_id = item.attr('data-tableId');
    if($(this).children('span').text() == 'IS TABLE'){
        console.log('You clicked IS TABLE');
        result = 'IS_TABLE'
    } else{
        console.log('You clicked IS NOT TABLE');
        result = 'NOT_TABLE'
    }

    if(result){
        $.ajax({
            method: 'POST',
            url: "add_results.php",
            data : {
                result: result,
                name: name,
                table_id: table_id,
                user_email: localStorage.getItem('user_email')
            },
            success: function (res, success) {
                let data = JSON.parse(res);

                if(data.success){
                    setTimeout(
                        function()
                        {
                            getTables();
                        }, 100);

                    // $('.carousel').carousel('next');
                } else{
                    $('#exampleModal').modal({
                        backdrop: 'static',
                        keyboard: false
                    })
                }
            }
        });
    } else{
        alert('You need to click on the btn to proceed');
    }
});

$(document).ready(function(){

    $('#loader').hide();
    $('#username').modal({
        backdrop: 'static',
        keyboard: false
    });

    $("#user-form").submit(function(e){
        e.preventDefault();
    });

    $(document).on('click', '#close-model', function () {
        window.close();
    });

    $(document).on('click', '#load-more', function () {
        location.reload();
    });

    $(document).on('click', '#save-user', function () {
        let email = $('input[name=student-email]')[0].value;
		if(validateEmail(email)){
			localStorage.setItem('user_email',email);
			$('#username').toggle('close');
			getTables();
		} else{
			alert('Enter a valid email, Please!');
		}
    });
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function getTables() {
    $.get({
        url: "get_tables.php",
        data: {
            user_email: localStorage.getItem('user_email')
        },
        success: function (res, response) {
            var data = JSON.parse(res);
            if(data.success) {
                var markup = '';
                data.data.forEach(function (item) {
                    markup += `<div class="carousel-item" data-name="${item.name}" data-tableId="${item.table_id}">
                <img class="d-block w-100" src="img/${item.name}.png" alt="First slide">
                </div>`;

                });

                $('#carouselExampleSlidesOnly div.carousel-inner').html(markup);
                $('div.carousel-item').first().addClass('active');
                $('#main-content').show();


                $('div.modal-backdrop.fade.show').hide();

                $('#carouselExampleSlidesOnly').carousel({
                    interval: false
                });
                $('#carouselExampleSlidesOnly').carousel('pause');
            } else{
                alert(data.msg);
            }

        }
    });
}